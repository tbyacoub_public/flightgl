import { Scene, WebGLRenderer, DirectionalLight, PerspectiveCamera, Mesh } from "three";
export declare const SCENE: Scene;
export declare const LIGHT: DirectionalLight;
export declare const RENDERRER: WebGLRenderer;
export declare const CAMERA: PerspectiveCamera;
export declare const HIT_BOX: Mesh;
