
export const YAW_INTENSITY = 1;
export const ROLL_INTENSITY = 2;
export const PITCH_INTENSITY = 3;

export const YAW_ON_AXIS_INTENSITY = 15;
export const PITCH_ON_AXIS_INTENSITY = 30;
export const ROLL_ON_AXIS_INTENSITY = 30;

export const DISTANCE = -250;
export const DISTANCE_MULTIPLYIER = 100;

export const MAX_ROTATION = 15.0 / 360.0;

export const TIMESTAMP =  1000 / 60;
export const MAX_FPS = 60;
export const LOOP_MULTIPLIER = 1000;
export const MAX_STEPS = 240;