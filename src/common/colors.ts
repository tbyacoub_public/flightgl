import { Color } from "three";

export const BACKGROUND_COLOR: Color = new Color('rgb(0,0,0)');
export const DIRECTIONAL_LIGHT_COLOR = 0xffffff;