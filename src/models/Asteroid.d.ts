import * as THREE from 'three';
import { Texture } from 'three';
export declare class Asteroid extends THREE.Mesh {
    constructor(texture: Texture);
}
